# Contributor: Celeste <cielesti@protonmail.com>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=rime-ls
pkgver=0.2.4
pkgrel=0
pkgdesc="Language server for Rime input method engine"
url="https://github.com/wlh320/rime-ls"
arch="all"
license="BSD-3-Clause"
_llvmver=17 # keep in sync with main/rust
makedepends="
	cargo
	cargo-auditable
	clang$_llvmver-libclang
	librime-dev
	"
checkdepends="rime-plum-data"
source="https://github.com/wlh320/rime-ls/archive/v$pkgver/rime-ls-$pkgver.tar.gz
	Cargo.lock
	"

prepare() {
	default_prepare

	cp -v "$srcdir"/Cargo.lock .

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
}

check() {
	cargo test --frozen
}

package() {
	install -Dvm755 target/release/rime_ls \
		-t "$pkgdir"/usr/bin/
}

sha512sums="
3b9451af802d0e5a56c62eeb33c2022c10cd9196fab980ec8e1dea6014b92ffe0d03264115c8b6c345b19ae7f67507d604665bb795e11a27af4149f620bca9c7  rime-ls-0.2.4.tar.gz
5fe5ee2b0204d8167fc530277f8013f21982ce1acd7bbbdea05e86e26009a4387a851107b55e6641b5945d01cd95d2b8cc8772c43c73d0bc7ea9b40b90156227  Cargo.lock
"

# Maintainer: Wesley van Tilburg <justwesley@protonmail.com>
pkgname=minify
pkgver=2.20.20
pkgrel=0
pkgdesc="Minifier CLI for HTML, CSS, JS, JSON, SVG and XML"
url="https://github.com/tdewolff/minify"
arch="all"
options="net"
license="MIT"
makedepends="go bash"
subpackages="$pkgname-bash-completion"
source="$pkgname-$pkgver.tar.gz::https://github.com/tdewolff/minify/archive/v$pkgver.tar.gz"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	mkdir build
	go build -o build ./cmd/minify
}

check() {
	go test ./...
}

package() {
	install -Dm755 ./build/minify -t "$pkgdir"/usr/bin
	install -Dm644 ./cmd/minify/bash_completion "$pkgdir"/usr/share/bash-completion/completions/minify
}

sha512sums="
608aec858bd468b6606e0112bbda4c6c3007ee0a96bc7d718de036258c233f46f65b4edea274114eb11c28d4b89283658a975509f57220986b9b1fa91949d72a  minify-2.20.20.tar.gz
"

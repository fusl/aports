# Maintainer: Simon Zeni <simon@bl4ckb0ne.ca>
pkgname=mold
pkgver=2.30.0
pkgrel=0
pkgdesc="fast modern linker"
url="https://github.com/rui314/mold"
arch="all"
license="MIT"
_llvmver=17
makedepends="
	clang$_llvmver
	cmake
	linux-headers
	llvm$_llvmver-dev
	mimalloc2-dev
	onetbb-dev
	openssl-dev
	samurai
	xxhash-dev
	zlib-dev
	zstd-dev
	"
checkdepends="
	bash
	dwarf-tools
	grep
	perl
	"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/rui314/mold/archive/refs/tags/v$pkgver.tar.gz
	$pkgname-test-fix.patch
	$pkgname-ppc64le-test-skip.patch
	xxhash.patch
	"

case "$CARCH" in
s390x)
	# copyrel, shared-abs-sym, tls-large-alignment, tls-small-alignment
	options="!check"
	;;
esac
case "$CARCH" in
s390x|riscv64)
	;;
*)
	makedepends="$makedepends lld"
	export LDFLAGS="$LDFLAGS -fuse-ld=lld"
	;;
esac

build() {
	CC=clang CXX=clang++ \
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_DISABLE_PRECOMPILE_HEADERS=ON \
		-DMOLD_LTO=ON \
		-DMOLD_USE_SYSTEM_MIMALLOC=ON \
		-DMOLD_USE_SYSTEM_TBB=ON \
		-DBUILD_TESTING="$(want_check && echo ON || echo OFF)"

	cmake --build build
}

check() {
	ctest --output-on-failure --test-dir build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7cfba4f0fb332799ad267d3eafb8e2f0057af4484467b3e3fbaf8044220163a2c7e26cd1786510f250844c8b57e30c15167c8dd9688af1773abc580c5605abf3  mold-2.30.0.tar.gz
956997757fad80e01cb70dfb927477dbceac2c46874156fddbf32519ef2ee1f73e3ee9179a74f4c5bb02031ee4685f4dd92993a8f35637d0d99098036ebccd06  mold-test-fix.patch
07039b0126cc5471fd91363c83a979d7e906b001928215e23afe4ff659914c21979038353d15320ad53cbfdb2e71d185cd584774c86051c3bcf0b91f36db7f5d  mold-ppc64le-test-skip.patch
9a54c572df99c79e501806ad08cf5e0f5ef7a91f02c973c6e9a86980b1e1fadb0f028746f160bcf89933453a1854e481136ee2fbfb2dbde7f0f53b16e215bd71  xxhash.patch
"

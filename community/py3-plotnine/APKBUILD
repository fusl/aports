# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-plotnine
pkgver=0.13.5
pkgrel=0
pkgdesc="A grammar of graphics for Python"
url="https://github.com/has2k1/plotnine"
arch="noarch"
license="MIT"
depends="
	python3
	py3-matplotlib
	py3-mizani
	py3-numpy
	py3-pandas
	py3-patsy
	py3-scipy
	py3-statsmodels
	"
checkdepends="python3-dev py3-pytest py3-pytest-cov"
makedepends="py3-gpep517 py3-setuptools_scm py3-wheel"
subpackages="$pkgname-pyc"
source="https://github.com/has2k1/plotnine/archive/v$pkgver/plotnine-$pkgver.tar.gz"
builddir="$srcdir/plotnine-$pkgver"
options="!check" # disable for now | additional tests are failing in this major release

build() {
	export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	# assertion errors or missing dependencies
	local _skipped_tests="
		test_annotation_logticks
		and not test_coords
		and not test_facet_labelling
		and not test_facets
		and not test_geom_ribbon_area
		and not test_geom_segment
		and not test_geom_smooth
		and not test_geom_text_label
		and not test_scale_internals
		and not test_scale_labelling
		and not test_position
		and not test_theme"

	# shellcheck disable=SC2116
	python3 -m pytest -k "not $(echo $_skipped_tests)" --ignore=tests/test_geom_map.py
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/plotnine-$pkgver-py3-none-any.whl
}

sha512sums="
237abcb2e67f4352d38139746ff9d5e276fb2bb3baccaea8f9d5d43fd41eaba2659d1417582ab69064c0e47196495b62fe3f18fdefb5b918875961095e6b4aaf  plotnine-0.13.5.tar.gz
"
